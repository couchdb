#!/usr/bin/env ruby
#
#  Created by Matt Todd on 2007-10-25.
#  Copyright (c) 2007. All rights reserved.

# dependencies

['server', 'db', 'doc'].each do |req|
  require File.join(File.dirname(__FILE__), 'couchdb', req)
end

# module

module CouchDB
  
  VERSION = '0.0.1'
  
end
