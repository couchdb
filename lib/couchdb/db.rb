#!/usr/bin/env ruby
#--
#  Created by Matt Todd on 2007-10-25.
#  Copyright (c) 2007. All rights reserved.
#++

#--
# dependencies
#++

#

#--
# module
#++

module CouchDB
  class DB
    
    #--
    # constructor
    #++
    
    def initialize(serv, db)
      @serv = serv
      @db = db
    end
    
    #--
    # data access/support
    #++
    
    attr_reader :db
    
    def docs
      docs_ids.map { |id| doc(id) }
      # @serv.get("/#{@db}/_all_docs")["rows"].collect { |row| doc(row["id"]) }
    end
    alias_method :all, :docs
    alias_method :list, :docs
    
    def docs_ids
      @serv.get("/#{@db}/_all_docs")["rows"].collect { |row| row["id"] }
    end
    alias_method :all_ids, :docs_ids
    alias_method :list_ids, :docs_ids
    
    def doc(id)
      CouchDB::Doc.new(@serv, self, id, @serv.get("/#{@db}/#{id}"))
    end
    alias_method :"[]", :doc
    alias_method :"/", :doc
    
    #--
    # tests
    #++
    
    def self.exists?(serv, db)
      serv.get("/#{db}/_all_docs") rescue return false
      true
    end
    
    #--
    # irb helper
    #++
    
    def inspect
      sprintf("#<%s:%#0x DB:%s>", self.class.to_s, self.object_id, self.to_s)
    end
    
    def to_s
      "/#{@db}/"
    end
    
  end
end
