#!/usr/bin/env ruby
#--
#  Created by Matt Todd on 2007-10-25.
#  Copyright (c) 2007. All rights reserved.
#++

#--
# dependencies
#++

require 'net/http'
require 'uri'
require 'rubygems'
require 'json'

#--
# module
#++

module CouchDB
  class Server
    
    #--
    # constructor
    #++
    
    # Accepts the CouchDB URI and options for caching and active verses passive queueing.
    # 
    # NOTE: The +options+ param is currently NYI.
    
    def initialize(uri, options = {}, &block) # :yields: server
      @uri = URI.parse(uri)
      @options = options
      
      yield self if block
      self
    end
    
    #--
    # server communication
    #++
    
    def get(uri)
      request(Net::HTTP::Get.new(uri))
    end
    
    def post(uri, data)
      req = Net::HTTP::Post.new(uri)
      req["content-type"] = "application/json"
      req.body = data.to_json
      request(req)
    end
    
    def delete(uri)
      request(Net::HTTP::Delete.new(uri))
    end
    
    def put(uri, data)
      req = Net::HTTP::Put.new(uri)
      req["content-type"] = "application/json"
      req.body = data.to_json
      request(req)
    end
    
    # Handles actual HTTP requests which are constructed in the #get, #post, #delete, and #put methods.
    # The response from CouchDB is then parsed as JSON and returned to the appropriate method.
    
    def request(req)
      # prepare and send HTTP request
      res = Net::HTTP.start(@uri.host, @uri.port) { |http| http.request(req) }
      
      # handle non-successes
      handle_error(req, res) unless res.kind_of? Net::HTTPSuccess
      
      # parse response
      JSON.parse(res.body)
    end
    
    #--
    # data access/support
    #++
    
    # Fetches a list of all of the databases on the server.
    # 
    # call-seq:
    #   server.databases -> [...]
    #   server.all -> [...]
    #   server.list -> [...]
    
    def databases
      get('/_all_dbs').map { |db| CouchDB::DB.new(self, db) }
    end
    alias_method :all, :databases
    alias_method :list, :databases
    
    # Retrieves the specified database as a CouchDB::DB object.
    # 
    # call-seq:
    #   server.use(db) -> database
    #   server[db] -> database
    #   server/db -> database
    
    def use(db)
      if CouchDB::DB.exists?(self, db)
        CouchDB::DB.new(self, db)
      else
        add(db)
        use(db)
      end
    end
    alias_method :"[]", :use
    alias_method :"/", :use
    
    # Adds an empty database to the server.
    # 
    # call-seq:
    #   server.add(db) -> database
    #   server << db -> database
    
    def add(db)
      put("/#{db}", '')
      CouchDB::DB.new(self, db)
    end
    alias_method :create, :add
    alias_method :"<<", :add
    
    # Removes a database from the server.
    # 
    # call-seq:
    #   server.destroy(db) -> server
    #   server.remove(db) -> server
    #   server >> db -> server
    
    def destroy(db)
      delete("/#{db}")
      self
    end
    alias_method :remove, :destroy
    alias_method :">>", :destroy
    
    #--
    # irb helper
    #++
    
    def inspect
      sprintf("#<%s:%#0x URL:%s>", self.class.to_s, self.object_id, self.to_s)
    end
    
    def to_s
      "#{@uri.scheme}://#{@uri.host}:#{@uri.port}/"
    end
    
    
    
    private
    
    
    
    #--
    # error handlers
    #++
    
    def handle_error(req, res)
    	raise RuntimeError.new(res.to_json)
    	# raise RuntimeError.new("#{res.code}:#{res.message}\nMETHOD:#{req.method}\nURI:#{req.path}\n#{res.body}")
    end
    
  end
end
