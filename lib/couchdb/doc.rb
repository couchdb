#!/usr/bin/env ruby
#--
#  Created by Matt Todd on 2007-10-25.
#  Copyright (c) 2007. All rights reserved.
#++

#--
# dependencies
#++

#

#--
# module
#++

module CouchDB
  class Doc
    
    #--
    # constructor
    #++
    
    def initialize(serv, db, id, doc)
      @serv = serv
      @db = db
      @id = id
      @doc = doc
    end
    
    #--
    # data access/support
    #++
    
    attr_reader :id
    
    def fields
      @doc.keys
    end
    alias_method :columns, :fields
    alias_method :keys, :fields
    
    #--
    # content negotiation
    #++
    
    def value(field)
      @doc[field]
    end
    alias_method :key, :value
    alias_method :field, :value
    alias_method :"[]", :value
    alias_method :"/", :value
    
    #--
    # irb helper
    #++
    
    def inspect
      sprintf("#<%s:%#0x Doc:%s>", self.class.to_s, self.object_id, self.to_s)
    end
    
    def to_s
      "#{@db}#{@id}"
    end
    
  end
end
